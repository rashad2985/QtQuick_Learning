#include <QApplication>
#include <QtWidgets/QMessageBox>

#include "Handler.h"

Handler::Handler(QObject *parent) : QObject (parent) {
  QObject::connect(this, SIGNAL(colorChanged()), this, SLOT(handleColorChange()));
}

QString Handler::getColorName() {
  return m_colorName;
}

void Handler::setColorName(const QString &colorName) {
  m_colorName = colorName;
  emit colorChanged();
}

void Handler::handleColorChange() {
  int argc = 0;
  char *argv[1];
  QApplication app(argc, argv);
  QMessageBox msgBox;
  msgBox.setText("Color changed to " + getColorName().toUpper());
  msgBox.exec();
}

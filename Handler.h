#ifndef HANDLER_H
#define HANDLER_H

#include <QObject>
#include <QString>

class Handler : public QObject {
  Q_OBJECT
  Q_PROPERTY(QString colorName READ getColorName WRITE setColorName NOTIFY colorChanged)

public:
  explicit Handler(QObject *parent = nullptr);

  QString getColorName();
  void setColorName(const QString &colorName);

  void handleClick();

signals:
  void colorChanged();

public slots:
  void handleColorChange();

private:
  QString m_colorName;
};

#endif // HANDLER_H

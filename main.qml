import QtQuick 2.0
import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.1
import QtQuick.Window 2.0

ApplicationWindow {
    id: window
    visible: true
    width: 640
    height: 480
    title: qsTr("hello.world")

    Rectangle {
        id: page
        width: window.width; height: window.height

        MouseArea {
            id: mouseArea
            anchors.fill: parent
        }

        Text {
            id: helloText
            text: window.title
            anchors.top: page.top; anchors.topMargin: 4
            anchors.bottom: undefined; anchors.bottomMargin: undefined
            anchors.horizontalCenter: page.horizontalCenter
            font.pointSize: 24; font.bold: true

            states: State {
                name: "down"
                when: mouseArea.pressed
                AnchorChanges {
                    target: helloText
                    anchors.top: undefined
                    anchors.bottom: page.bottom
                }
                PropertyChanges {
                    target: helloText
                    anchors.topMargin: undefined
                    anchors.bottomMargin: 4
                    color: "red"
                }
            }

            transitions: Transition {
                from: ""
                to: "down"
                reversible: true
                ParallelAnimation {
                    AnchorAnimation {
                        duration: 1000
                    }
                    ColorAnimation {
                        duration: 1000
                    }
                    PropertyAnimation {
                        duration: 1000
                    }
                }
            }
        }


        Grid {
            id: colorPicker
            x: 4
            anchors.bottom: page.bottom
            anchors.bottomMargin: 4
            rows: 2; columns: 3; spacing: 3

            Cell {cellColor: "red"; onClicked: helloText.color = cellColor}
            Cell {cellColor: "green"; onClicked: helloText.color = cellColor}
            Cell {cellColor: "blue"; onClicked: helloText.color = cellColor}
            Cell {cellColor: "yellow"; onClicked: helloText.color = cellColor}
            Cell {cellColor: "steelblue"; onClicked: helloText.color = cellColor}
            Cell {cellColor: "black"; onClicked: helloText.color = cellColor}
        }
    }

    Pane {
        width: 120
        height: 120

        Material.elevation: 6

        Label {
            text: qsTr("I'm a card!")
            anchors.centerIn: parent
        }
    }

    Button {
        id: x
        text: qsTrId("close")
        onClicked: Qt.quit()

        anchors.top: page.top
        anchors.topMargin: 4
        anchors.right: page.right
        anchors.rightMargin: 4
    }
}

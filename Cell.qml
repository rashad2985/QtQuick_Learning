import QtQuick 2.0

import de.asgarbayli.rashad.handler 1.0

Item {
    id: container
    property alias cellColor: rectangle.color
    signal clicked(color cellColor)
    width: 40; height: 25

    Handler {
        id: handler
    }

    Rectangle {
        id: rectangle
        border.color: "white"
        anchors.fill: parent
    }

    signal clickedColorPicker()
    onClickedColorPicker: {
        console.log(container.cellColor + " clicked")
        container.clicked(container.cellColor)
        handler.colorName = container.cellColor
    }

    MouseArea {
        anchors.fill: parent
        onClicked: clickedColorPicker()
    }
}

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QTranslator>

#include "Handler.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QTranslator translator;
    // look up e.g. :/translation/texts_en.qm
    if (translator.load(QLocale(), QLatin1String("texts"), QLatin1String("_"), QLatin1String(":/translation"))) {
        app.installTranslator(&translator);
        QCoreApplication::installTranslator(&translator);
    }

    qmlRegisterType<Handler>("de.asgarbayli.rashad.handler", 1, 0, "Handler");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
